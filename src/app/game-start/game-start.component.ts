import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {GameService} from "../game.service";

@Component({
  selector: 'game-start',
  templateUrl: './game-start.component.html',
  styleUrls: ['./game-start.component.css']
})
export class GameStartComponent implements OnInit {

  constructor(private gameService: GameService, private router: Router) { }

  ngOnInit() {
  }

  startGame() {
    this.gameService.deleteGameFromLocalStorage();
    this.router.navigate(["/play"]);
  }

}
