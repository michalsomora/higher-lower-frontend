import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { GameStartComponent } from './game-start/game-start.component';
import { GameRoundComponent } from './game-round/game-round.component';
import { GameResultComponent } from './game-result/game-result.component';
import { GameHeaderComponent } from './game-header/game-header.component';
import { GameService } from 'app/game.service';
import { UserService } from 'app/user.service';

const appRoutes: Routes = [
  {path: 'start', component: GameStartComponent},
  {path: 'play', component: GameRoundComponent},
  {path: 'result', component: GameResultComponent},
  {path: '', redirectTo: "start", pathMatch: 'full'}
]

@NgModule({
  declarations: [
    AppComponent,
    GameStartComponent,
    GameRoundComponent,
    GameResultComponent,
    GameHeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [UserService, GameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
