import { Injectable } from '@angular/core';

@Injectable()
export class UserService {

  constructor() { }

  getUser(): string {
    const user = localStorage.getItem("user") || null;
    return user;
  }

  setUser(user: string): void {
    localStorage.setItem("user", user);
  }

  deleteUser(): void{
    localStorage.removeItem("user");
  }

}
