import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs/Observable";

@Injectable()
export class GameService {

  constructor(private http: HttpClient) { }

  startGame(): any {
    const url = "http://localhost:8080/api/game/start";
    return this.http.get(url);
  }

  sendGuess(id: number, guess: number): any {
    const url = "http://localhost:8080/api/game/guess";
    const data = {id, guess};
    return this.http.post(url, data);
  }

  saveGameStateToLocalStorage(gameObj){
    localStorage.setItem("game", JSON.stringify(gameObj));
  }

  deleteGameFromLocalStorage(){
    localStorage.removeItem("game");
  }

  getGame() {
    return JSON.parse(localStorage.getItem("game"));
  }

}
