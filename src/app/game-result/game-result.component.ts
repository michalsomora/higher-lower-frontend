import { Component, OnInit } from '@angular/core';
import { GameService  } from '../game.service';
import { Router } from '@angular/router';

@Component({
  selector: 'game-result',
  templateUrl: './game-result.component.html',
  styleUrls: ['./game-result.component.css']
})
export class GameResultComponent implements OnInit {
  hitsCount: number;
  lastNumber: number;
  gameWon: boolean;

  constructor(private gameService: GameService, private router: Router) { }

  ngOnInit() {
    const gameObj = this.gameService.getGame();
    if (gameObj) {
      this.lastNumber = gameObj.last;
      this.hitsCount = gameObj.round;
      this.gameWon = this.hitsCount === 5;
    }

    this.gameService.deleteGameFromLocalStorage();
  }

  restartGame() {
    this.router.navigate(["/"]);
  }
}
