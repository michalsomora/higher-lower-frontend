import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'game-header',
  templateUrl: './game-header.component.html',
  styleUrls: ['./game-header.component.css']
})
export class GameHeaderComponent implements OnInit {

  public user: string;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.user = this.userService.getUser();
  }

  saveUser(name) {
    this.user = name;
    this.userService.setUser(name);
  }

  changeUser() {
    this.user = null;
    this.userService.deleteUser();
  }
}
