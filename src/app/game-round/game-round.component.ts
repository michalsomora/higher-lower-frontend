import { Component, OnInit } from '@angular/core';
import { GameService } from '../game.service';
import { Router } from '@angular/router';


@Component({
  selector: 'game-round',
  templateUrl: './game-round.component.html',
  styleUrls: ['./game-round.component.css']
})
export class GameRoundComponent implements OnInit {
  public lastNumber: number;
  public gameId: number;
  public gameRound: number;

  constructor(private gameService: GameService, private router: Router) { }

  ngOnInit() {
    const gameObj = this.gameService.getGame();
    if(gameObj){
      this.setState(gameObj.last, gameObj.id, gameObj.round);
    } else {
      this.gameService.startGame().subscribe(res => {
        this.setState(res.last, res.id, res.round);
        this.gameService.saveGameStateToLocalStorage(res);
      });
    }
  }

  guessNumber(guess: number){
    this.gameService.sendGuess(this.gameId, guess).subscribe(res => {
      const status = res.status;
      this.setState(res.last, res.id, res.round);
      this.gameService.saveGameStateToLocalStorage(res);

      if(status === "WIN" || status === "LOOSE"){
        this.router.navigate(["/result"]);
      }
    });
  }

  private setState(lastNumber, gameId, gameRound?){
    this.lastNumber = lastNumber;
    this.gameId = gameId;
    this.gameRound = gameRound;
  }
}
