import { browser, element, by } from 'protractor';

export class HigherOrLowerPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }

  getStartGameButton() {
    return element(by.css('.btn-start'));
  }

  getGameRoundPageParagraph() {
    return element(by.css('.round-info h3.round-question')).getText();
  }

  getHigherButton() {
    return element(by.css('.btn-higher'));
  }

  getLowerButton() {
    return element(by.css('.btn-lower'));
  }

  getUserEditLink(){
    return element(by.css('.user-link'));
  }

  getUserEditInput(){
    return element(by.css('.user-name-input'));
  }

  getUserEditButton(){
    return element(by.css('.btn-small'));
  }

  getUserName(){
    return element(by.css('.user-name')).getText();
  }
}
