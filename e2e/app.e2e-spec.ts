import { HigherOrLowerPage } from './app.po';
import { WebDriver } from 'selenium-webdriver';
import { browser } from 'protractor';
import { protractor } from 'protractor/built/ptor';

describe('higher-or-lower App', () => {
  let page: HigherOrLowerPage;

  beforeEach(() => {
    page = new HigherOrLowerPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Higher or lower?');
  });

  it('should display Start Game button', () => {
    expect(page.getStartGameButton().isDisplayed());
  });

  it('should click on Start Game button', () => {
    browser.sleep(1000);
    expect(page.getStartGameButton().click());
  });

  it('should display Game Round page', () => {
    browser.sleep(1000);
    expect(page.getGameRoundPageParagraph()).toEqual('Will the next number be higher or lower?');
  });

  it('should display input field and save button', () => {
    page.navigateTo();
    expect(page.getUserEditInput().isDisplayed());
    expect(page.getUserEditButton().isDisplayed());
  });

  it('should fill in a new user name and display it', () => {
    browser.sleep(1000);
    page.getUserEditInput().sendKeys("TestName");
    page.getUserEditButton().click();
    expect(page.getUserName()).toEqual('TestName');
  });

  it('should display input and button after click on edit link', () => {
    browser.sleep(1000);
    page.getUserEditLink().click();
    expect(page.getUserEditInput().isDisplayed());
    expect(page.getUserEditButton().isDisplayed());
  });

  it('should save user name after edit and display it', () => {
    browser.sleep(1000);
    page.getUserEditInput().sendKeys("TestName2");
    page.getUserEditButton().click();
    expect(page.getUserName()).toEqual('TestName2');
  });
});
